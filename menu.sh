#!/bin/bash
OPTIONS="Start Quit"
echo "Bienvenue, veuillez saisir le numéro correspondant pour choisir une fonctionnalité ci-dessous"

select opt in $OPTIONS
do
if [ "$opt" = "Quit" ]
then
	echo "Au revoir"
	exit
elif [ "$opt" = "Start" ]
then
	./main.sh
else
	clear
	echo bad option
fi
done
