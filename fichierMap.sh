#!/bin/bash
createMap() {
local dossTemp=$(mktemp -d)

let "but=2"

for i in $(seq 1 23)
do 
	mkdir $dossTemp/ligne$i
	for j in $(seq 1 23)
	do 
		touch $dossTemp/ligne$i/colonne$j
	done
done


#Ligne1
for i in $(seq 1 23)
do 
	echo "#" > $dossTemp/ligne1/colonne$i
done

#Ligne2
echo "#" > $dossTemp/ligne2/colonne1
echo "#" > $dossTemp/ligne2/colonne12
echo "#" > $dossTemp/ligne2/colonne23
for i in $(seq 2 11)
do 
	echo "*" > $dossTemp/ligne2/colonne$i
done
for i in $(seq 13 22)
do 
	echo "*" > $dossTemp/ligne2/colonne$i
done

#Ligne3
echo "#" > $dossTemp/ligne3/colonne1
echo "#" > $dossTemp/ligne3/colonne12
echo "#" > $dossTemp/ligne3/colonne23
echo "O" > $dossTemp/ligne3/colonne2
echo "*" > $dossTemp/ligne3/colonne6
echo "*" > $dossTemp/ligne3/colonne11
echo "*" > $dossTemp/ligne3/colonne13
echo "*" > $dossTemp/ligne3/colonne18
echo "O" > $dossTemp/ligne3/colonne22
for i in $(seq 3 5)
do 
	echo "#" > $dossTemp/ligne3/colonne$i
done
for i in $(seq 7 10)
do 
	echo "#" > $dossTemp/ligne3/colonne$i
done
for i in $(seq 14 17)
do 
	echo "#" > $dossTemp/ligne3/colonne$i
done
for i in $(seq 19 21)
do 
	echo "#" > $dossTemp/ligne3/colonne$i
done

#Ligne4
echo "#" > $dossTemp/ligne4/colonne1
echo "#" > $dossTemp/ligne4/colonne12
echo "#" > $dossTemp/ligne4/colonne23
echo "O" > $dossTemp/ligne4/colonne2
echo "*" > $dossTemp/ligne4/colonne6
echo "*" > $dossTemp/ligne4/colonne11
echo "*" > $dossTemp/ligne4/colonne13
echo "*" > $dossTemp/ligne4/colonne18
echo "O" > $dossTemp/ligne4/colonne22
for i in $(seq 3 5)
do 
	echo "#" > $dossTemp/ligne4/colonne$i
done
for i in $(seq 7 10)
do 
	echo "#" > $dossTemp/ligne4/colonne$i
done
for i in $(seq 14 17)
do 
	echo "#" > $dossTemp/ligne4/colonne$i
done
for i in $(seq 19 21)
do 
	echo "#" > $dossTemp/ligne4/colonne$i
done

#Ligne5
echo "#" > $dossTemp/ligne5/colonne1
echo "#" > $dossTemp/ligne5/colonne23
for i in $(seq 2 22)
do 
	echo "*" > $dossTemp/ligne5/colonne$i
done

#Ligne6
echo "#" > $dossTemp/ligne6/colonne1
echo "#" > $dossTemp/ligne6/colonne23
echo "#" > $dossTemp/ligne6/colonne7
echo "#" > $dossTemp/ligne6/colonne17
echo "*" > $dossTemp/ligne6/colonne2
echo "*" > $dossTemp/ligne6/colonne6
echo "*" > $dossTemp/ligne6/colonne8
echo "*" > $dossTemp/ligne6/colonne16
echo "*" > $dossTemp/ligne6/colonne18
echo "*" > $dossTemp/ligne6/colonne22

for i in $(seq 3 5)
do 
	echo "#" > $dossTemp/ligne6/colonne$i
done

for i in $(seq 9 15)
do 
	echo "#" > $dossTemp/ligne6/colonne$i
done

for i in $(seq 19 21)
do 
	echo "#" > $dossTemp/ligne6/colonne$i
done

#Ligne7
echo "#" > $dossTemp/ligne7/colonne1
echo "#" > $dossTemp/ligne7/colonne23
echo "#" > $dossTemp/ligne7/colonne7
echo "#" > $dossTemp/ligne7/colonne17
echo "#" > $dossTemp/ligne7/colonne12
echo "*" > $dossTemp/ligne7/colonne2
echo "*" > $dossTemp/ligne7/colonne6
echo "*" > $dossTemp/ligne7/colonne8
echo "*" > $dossTemp/ligne7/colonne16
echo "*" > $dossTemp/ligne7/colonne18
echo "*" > $dossTemp/ligne7/colonne22

for i in $(seq 3 5)
do 
	echo "*" > $dossTemp/ligne7/colonne$i
done

for i in $(seq 9 11)
do 
	echo "#" > $dossTemp/ligne7/colonne$i
done

for i in $(seq 13 15)
do 
	echo "#" > $dossTemp/ligne7/colonne$i
done

for i in $(seq 19 21)
do 
	echo "*" > $dossTemp/ligne7/colonne$i
done

#Ligne8
echo "#" > $dossTemp/ligne8/colonne7
echo "#" > $dossTemp/ligne8/colonne12
echo "#" > $dossTemp/ligne8/colonne17
echo "*" > $dossTemp/ligne8/colonne6
echo "*" > $dossTemp/ligne8/colonne18
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne8/colonne$i
done
for i in $(seq 8 11)
do 
	echo "*" > $dossTemp/ligne8/colonne$i
done
for i in $(seq 13 16)
do 
	echo "*" > $dossTemp/ligne8/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne8/colonne$i
done

#Ligne9
echo "#" > $dossTemp/ligne9/colonne12
echo "*" > $dossTemp/ligne9/colonne6
echo "*" > $dossTemp/ligne9/colonne18
echo " " > $dossTemp/ligne9/colonne11
echo " " > $dossTemp/ligne9/colonne13
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne9/colonne$i
done
for i in $(seq 7 10)
do 
	echo "#" > $dossTemp/ligne9/colonne$i
done
for i in $(seq 14 17)
do 
	echo "#" > $dossTemp/ligne9/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne9/colonne$i
done

#Ligne10
echo "#" > $dossTemp/ligne10/colonne7
echo " " > $dossTemp/ligne10/colonne12
echo "#" > $dossTemp/ligne10/colonne17
echo "*" > $dossTemp/ligne10/colonne6
echo "*" > $dossTemp/ligne10/colonne18
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne10/colonne$i
done
for i in $(seq 8 11)
do 
	echo " " > $dossTemp/ligne10/colonne$i
done
for i in $(seq 13 16)
do 
	echo " " > $dossTemp/ligne10/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne10/colonne$i
done

#Ligne11
echo "#" > $dossTemp/ligne11/colonne7
echo " " > $dossTemp/ligne11/colonne12
echo " " > $dossTemp/ligne11/colonne8
echo " " > $dossTemp/ligne11/colonne16
echo "#" > $dossTemp/ligne11/colonne17
echo "*" > $dossTemp/ligne11/colonne6
echo "*" > $dossTemp/ligne11/colonne18
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne11/colonne$i
done
for i in $(seq 9 11)
do 
	echo "#" > $dossTemp/ligne11/colonne$i
done
for i in $(seq 13 15)
do 
	echo "#" > $dossTemp/ligne11/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne11/colonne$i
done

#Ligne12
echo "*" > $dossTemp/ligne12/colonne6
echo "*" > $dossTemp/ligne12/colonne18
echo "#" > $dossTemp/ligne12/colonne9
echo "#" > $dossTemp/ligne12/colonne15
for i in $(seq 1 5)
do 
	echo " " > $dossTemp/ligne12/colonne$i
done
for i in $(seq 7 8)
do 
	echo " " > $dossTemp/ligne12/colonne$i
done
for i in $(seq 10 14)
do 
	echo " " > $dossTemp/ligne12/colonne$i
done
for i in $(seq 16 17)
do 
	echo " " > $dossTemp/ligne12/colonne$i
done
for i in $(seq 19 23)
do 
	echo " " > $dossTemp/ligne12/colonne$i
done

#Ligne13
echo "#" > $dossTemp/ligne13/colonne7
echo " " > $dossTemp/ligne13/colonne12
echo " " > $dossTemp/ligne13/colonne8
echo " " > $dossTemp/ligne13/colonne16
echo "#" > $dossTemp/ligne13/colonne17
echo "*" > $dossTemp/ligne13/colonne6
echo "*" > $dossTemp/ligne13/colonne18
echo "#" > $dossTemp/ligne13/colonne12
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne13/colonne$i
done
for i in $(seq 9 11)
do 
	echo "#" > $dossTemp/ligne13/colonne$i
done
for i in $(seq 13 15)
do 
	echo "#" > $dossTemp/ligne13/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne13/colonne$i
done

#Ligne14
echo "#" > $dossTemp/ligne14/colonne7
echo " " > $dossTemp/ligne14/colonne12
echo "#" > $dossTemp/ligne14/colonne17
echo "*" > $dossTemp/ligne14/colonne6
echo "*" > $dossTemp/ligne14/colonne18
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne14/colonne$i
done
for i in $(seq 8 11)
do 
	echo " " > $dossTemp/ligne14/colonne$i
done
for i in $(seq 13 16)
do 
	echo " " > $dossTemp/ligne14/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne14/colonne$i
done
#Ligne15
echo "#" > $dossTemp/ligne15/colonne12
echo "*" > $dossTemp/ligne15/colonne6
echo "*" > $dossTemp/ligne15/colonne18
echo " " > $dossTemp/ligne15/colonne11
echo " " > $dossTemp/ligne15/colonne13
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne15/colonne$i
done
for i in $(seq 7 10)
do 
	echo "#" > $dossTemp/ligne15/colonne$i
done
for i in $(seq 14 17)
do 
	echo "#" > $dossTemp/ligne15/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne15/colonne$i
done

#Ligne16
echo "#" > $dossTemp/ligne16/colonne7
echo "#" > $dossTemp/ligne16/colonne12
echo "#" > $dossTemp/ligne16/colonne17
echo "*" > $dossTemp/ligne16/colonne6
echo "*" > $dossTemp/ligne16/colonne18
for i in $(seq 1 5)
do 
	echo "#" > $dossTemp/ligne16/colonne$i
done
for i in $(seq 8 11)
do 
	echo "*" > $dossTemp/ligne16/colonne$i
done
for i in $(seq 13 16)
do 
	echo "*" > $dossTemp/ligne16/colonne$i
done
for i in $(seq 19 23)
do 
	echo "#" > $dossTemp/ligne16/colonne$i
done
#Ligne17
echo "#" > $dossTemp/ligne17/colonne1
echo "#" > $dossTemp/ligne17/colonne23
echo "#" > $dossTemp/ligne17/colonne7
echo "#" > $dossTemp/ligne17/colonne17
echo "#" > $dossTemp/ligne17/colonne12
echo "*" > $dossTemp/ligne17/colonne2
echo "*" > $dossTemp/ligne17/colonne6
echo "*" > $dossTemp/ligne17/colonne8
echo "*" > $dossTemp/ligne17/colonne16
echo "*" > $dossTemp/ligne17/colonne18
echo "*" > $dossTemp/ligne17/colonne22

for i in $(seq 3 5)
do 
	echo "*" > $dossTemp/ligne17/colonne$i
done

for i in $(seq 9 11)
do 
	echo "#" > $dossTemp/ligne17/colonne$i
done

for i in $(seq 13 15)
do 
	echo "#" > $dossTemp/ligne17/colonne$i
done
for i in $(seq 19 21)
do 
	echo "*" > $dossTemp/ligne17/colonne$i
done

#Ligne18
echo "#" > $dossTemp/ligne18/colonne1
echo "#" > $dossTemp/ligne18/colonne23
echo "#" > $dossTemp/ligne18/colonne7
echo "#" > $dossTemp/ligne18/colonne17
echo "*" > $dossTemp/ligne18/colonne2
echo "*" > $dossTemp/ligne18/colonne6
echo "*" > $dossTemp/ligne18/colonne8
echo "*" > $dossTemp/ligne18/colonne16
echo "*" > $dossTemp/ligne18/colonne18
echo "*" > $dossTemp/ligne18/colonne22

for i in $(seq 3 5)
do 
	echo "#" > $dossTemp/ligne18/colonne$i
done

for i in $(seq 9 15)
do 
	echo "#" > $dossTemp/ligne18/colonne$i
done

for i in $(seq 19 21)
do 
	echo "#" > $dossTemp/ligne18/colonne$i
done

#Ligne19
echo "#" > $dossTemp/ligne19/colonne1
echo "#" > $dossTemp/ligne19/colonne23
for i in $(seq 2 22)
do 
	echo "*" > $dossTemp/ligne19/colonne$i
done
#Ligne20
echo "#" > $dossTemp/ligne20/colonne1
echo "#" > $dossTemp/ligne20/colonne12
echo "#" > $dossTemp/ligne20/colonne23
echo "*" > $dossTemp/ligne20/colonne2
echo "*" > $dossTemp/ligne20/colonne6
echo "*" > $dossTemp/ligne20/colonne11
echo "*" > $dossTemp/ligne20/colonne13
echo "*" > $dossTemp/ligne20/colonne18
echo "*" > $dossTemp/ligne20/colonne22
for i in $(seq 3 5)
do 
	echo "#" > $dossTemp/ligne20/colonne$i
done
for i in $(seq 7 10)
do 
	echo "#" > $dossTemp/ligne20/colonne$i
done
for i in $(seq 14 17)
do 
	echo "#" > $dossTemp/ligne20/colonne$i
done
for i in $(seq 19 21)
do 
	echo "#" > $dossTemp/ligne20/colonne$i
done
#Ligne21
echo "#" > $dossTemp/ligne21/colonne1
echo "#" > $dossTemp/ligne21/colonne12
echo "#" > $dossTemp/ligne21/colonne23
echo "O" > $dossTemp/ligne21/colonne2
echo "*" > $dossTemp/ligne21/colonne6
echo "*" > $dossTemp/ligne21/colonne11
echo "*" > $dossTemp/ligne21/colonne13
echo "*" > $dossTemp/ligne21/colonne18
echo "O" > $dossTemp/ligne21/colonne22
for i in $(seq 3 5)
do 
	echo "#" > $dossTemp/ligne21/colonne$i
done
for i in $(seq 7 10)
do 
	echo "#" > $dossTemp/ligne21/colonne$i
done
for i in $(seq 14 17)
do 
	echo "#" > $dossTemp/ligne21/colonne$i
done
for i in $(seq 19 21)
do 
	echo "#" > $dossTemp/ligne21/colonne$i
done

#Ligne22
echo "#" > $dossTemp/ligne22/colonne1
echo "#" > $dossTemp/ligne22/colonne12
echo "#" > $dossTemp/ligne22/colonne23
for i in $(seq 2 11)
do 
	echo "*" > $dossTemp/ligne22/colonne$i
done
for i in $(seq 13 22)
do 
	echo "*" > $dossTemp/ligne22/colonne$i
done
#Ligne23

for i in $(seq 1 23)
do 
	echo "#" > $dossTemp/ligne23/colonne$i
done


echo $dossTemp
}
