#!/bin/bash

# Initialiser les 3 caractéristiques de Pacman en variable globale
initPac() {
	## X : La position X de Pacman
	let "X=19"

	## Y : La position Y de Pacman
	let "Y=12"

	## Dir : La direction vers laquelle se dirige Pacman
	### (haut/bas/gauche/droite)
	D="droite"
}
