#!/bin/bash

afficheMap() {
nblignes=23
nbcol=23
jaunepac='\e[1;33m'
bleumur='\e[0;34m'
blanc='\e[1;37m'

echo > affichage
echo "PACMAN --- SCORE : $score" >> affichage


for i in $(seq 1 "$nblignes")
do
	for j in $(seq 1 "$nbcol")
	do
		if [ $(($X)) -eq $(($j)) ] && [ $(($Y)) -eq $(($i)) ]
		then
			echo -e -n "${jaunepac}@" >> affichage
		else
			caractere=$(cat "$map/ligne$i/colonne$j")
			if [ "$caractere" == "#" ]
			then
				echo -e -n "${bleumur}$caractere" >> affichage
			else
				echo -e -n "${blanc}$caractere" >> affichage
			fi


		fi
	done
echo >> affichage
done

clear
cat affichage
}

