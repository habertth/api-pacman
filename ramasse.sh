#!/bin/bash

#Vérifier si la case est un point non ramassé
IsPoint()
{
	## PARAMETRES
	local caractere="$(cat $map/ligne$2/colonne$1)"
	if [ "$caractere" == "*" ]
	then
		echo 1
	else
		echo 0
	fi
}

Ramasser(){
	if [ $(IsPoint "$X" "$Y") == "1" ]
	then
		score=$(($score+1))
		echo " " > $map/ligne$Y/colonne$X
	fi
}
