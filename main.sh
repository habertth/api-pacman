#!/bin/bash

# Incorporer les différentes fonctions
source afficheMap.sh
source fichierMap.sh
source finDePartie.sh
source initPac.sh
source input.sh
source movePac.sh
source ramasse.sh
source ramasse_pw.sh

# Inititialisation
map=$(createMap)
initPac
hasWon=0
score=0
but=232


#Boucle de jeu
## Tant que le joueur n'a pas gagné
while [ "$hasWon" == 0 ]
do
	afficheMap
	getInput
	movePac
	Ramasser
	RamasserPw
	hasWon=$(finDePartie)
done

# Ecran de fin de jeu
clear
echo "#########################"
echo "####    GAME OVER    ####"
echo "#########################"
echo
enregistrerScore



