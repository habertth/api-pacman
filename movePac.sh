#!/bin/bash

#Vérifier si la case est libre (=n'est pas un mur)
notWall() {
	## PARAMETRES
	# - $1 x à tester
	# - $2 y à tester
	local caractere="$(cat $map/ligne$2/colonne$1)"
	if [ "$caractere" == "#" ]
	then
		echo 0
	else
		echo 1
	fi
}

# Déplacer pacman en fonction de la direction qu'il possède
movePac() {

	local "X2=$X"
	local "Y2=$Y"

	case "$D" in
	haut) 
		let "Y2=$(($Y2-1))";;
	bas) 
		let "Y2=$(($Y2+1))";;
	droite) 
		let "X2=$(($X2+1))";;
	gauche) 
		let "X2=$(($X2-1))";;
	esac

	if [ $(notWall "$X2" "$Y2") -eq 1 ]
	then
		X=$X2
		Y=$Y2
	fi
}
