#!/bin/bash

#Vérifier si la case est un power-up non ramassé
IsPower()
{
	## PARAMETRES
	local caractere="$(cat $map/ligne$2/colonne$1)"
	if [ "$caractere" == "O" ]
	then
		echo 1
	else
		echo 0
	fi
}

RamasserPw(){
	if [ $(IsPower "$X" "$Y") == "1" ]
	then
		score=$(($score+10))
		echo " " > $map/ligne$Y/colonne$X
	fi
}
